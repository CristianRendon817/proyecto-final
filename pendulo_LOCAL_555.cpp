#include "pendulo.h"
#include <QKeyEvent>


#include <QDebug>

//constructor
pendulo::pendulo(double angulo)
{
    px=80;
    py=0;
    this->setPos(px,py);
    set_theta(angulo);
    velo=0;

}

pendulo::pendulo(double angulo,double pox,double poy)
{
    px=pox;
    py=poy;
    this->setPos(px,py);
    set_theta(angulo);
    velo=0;

}

pendulo::pendulo(double angulo,double pox,double poy,double v)
{
    px=pox;
    py=poy;
    this->setPos(px,py);
    set_theta(angulo);
    velo=v;

}

//aceleracion angular
double pendulo::acel()
{
    double a;
    a = (-9.8*sin(to*3.1415926/180))/longitud;
    return a;
}

//velocidad angular
double pendulo::vel(double a)
{
    double v;
    v = vo + a*tiempo;
    vo = v; //se actualiza la velocidad
    return v;
}

//posicion angular
double pendulo::theta(double v, double a)
{
    double theta;
    theta = to + v*tiempo + (a*tiempo*tiempo)/2;
    to = theta;

    //qDebug()<<"angulo: "<<theto;
    return theta;

}

//SLOTS

QRectF pendulo::boundingRect() const
{
    return QRectF(px-10,py,20,220); //se configura la posicion y tamaño del pendulo (barra)
}


//para actualizar el elemento (pendulo)
void pendulo::advance(int phase)
{
    if(!phase)return;
    double a,v,p;
    a=acel();
    v=vel(a);
    p=theta(v,a);

    setTransformOriginPoint(px, py); //se cambia la posicion del eje de rotacion
    this->setRotation(p); //establece el angulo de rotación
}

void pendulo::paint(QPainter *painter, const QStyleOptionGraphicsItem *option,QWidget *widget)
{
    QBrush pincel(Qt::darkMagenta); //relleno
    QPen pen(Qt::black);            //borde
    pen.setWidthF(0.5);

    painter->setRenderHint(QPainter::Antialiasing); //para enfocar
    painter->setBrush(pincel); //se indica el Qbrush
    painter->setPen(pen); //se indica el pincel

    //Se dibujan las figuras
    painter->drawRect(px-2.5,py,5,220); //se dibuja la 'cuerda' del pendulo
    painter->drawEllipse(px-10,py+longitud,20,20); //se dibuja la bola en la parte inferior
}





